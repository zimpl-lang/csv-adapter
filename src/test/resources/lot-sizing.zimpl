# Single Item Lot-Sizing

set T ;
param N := card(T);	# time periods
param demand[T] ;   # demand for <t> in T
param cost[T] ;     # cost of unit production, for <t> in T
param max_prod;     # max production capacity
param max_stock;    # max stock in inventory
param safe_stock;   # safety stock
param s0;           # initial stock
var x[T] >= 0;
var s[{0}+T] >= 0;
min cost: sum <t> in T: cost[t]*x[t];
subto stock: forall <t> in T:
    s[t] == s[t-1] + x[t] - demand[t];
subto max_prod: forall <t> in T:
    x[t] <= max_prod;
subto max_stock: forall <t> in T:
    s[t] <= max_stock;
subto safe_stock: forall <t> in T:
    s[t] >= safe_stock;
subto init_stock: s[0] == s0;
