import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import tree.AST;
import zimpler.Resolver;
import zimpler.io.CsvLoader;
import zimpler.io.Loader;
import zimpler.io.PropertiesLoader;
import zimpler.io.ZimplVisitor;

class Tests {
	
	List<Loader<String>> loaders = 
			List.of(new CsvLoader(), new PropertiesLoader());
	
	@Test void lotSizing() throws IOException {
		AST model = new Resolver()
				.resolveFromFilename("lot-sizing.zimpl", loaders);
		String result = model.accept(new ZimplVisitor());
		System.out.println("csv-adapter/Tests/lotSizing.result:\n" + result);
		
//		fail("Not yet implemented"); // TODO
	}
	
}
