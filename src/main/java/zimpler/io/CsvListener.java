package zimpler.io;

import java.util.ArrayList;
import java.util.List;

import csv.antlr.CSVBaseListener;
import csv.antlr.CSVParser.FieldContext;
import csv.antlr.CSVParser.HdrContext;
import csv.antlr.CSVParser.RowContext;
import zimpler.Table;

/**
 * @author Javier MV
 * @since 0.1
 */
public class CsvListener extends CSVBaseListener {
	
	Table.Builder<String> builder;
	
	public CsvListener(String filename) {
		builder = new Table.Builder<String>(filename);
		// TODO: test empty header and oneliners
		// FIXME: the grammar does not accept comments; see if it is
		// convenient to extend or modify the grammar, or something else.
	}
	
	public Table<String> getResult() {
		return builder.getResult(); 
	}
	
	@Override public void exitHdr(HdrContext ctx) {
//		hdr : row ;
		builder.setFirstRowAsHeader();
	}
	
	@Override public void exitRow(RowContext ctx) {
//		row : field (',' field)* '\r'? '\n' ;
//		field : TEXT | STRING | ;
		List<String> values = new ArrayList<>();
		for (FieldContext field : ctx.field()) {
			String value = ""; // the empty string is a valid value
			if (field.STRING() != null)
				value = field.STRING().getText();
			else if (field.TEXT() != null)
				value = field.TEXT().getText();
			values.add(value);
		}
		builder.addRow(values.toArray(new String[0]));
	}
	
}
