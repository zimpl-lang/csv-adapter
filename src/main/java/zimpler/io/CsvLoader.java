package zimpler.io;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import csv.CsvParser;
import zimpler.Resolver;
import zimpler.Table;

/**
 * @author Javier MV
 * @since 0.1
 */
public class CsvLoader implements Loader<String> {
	
	@Override public String acceptedExtension() { return "csv"; }
	
	@Override public String extensionDescription() {
		return "Comma-separated values";
	}
	
	@Override
//	public Properties load(String filename) throws IOException {
	public Map<String, String> load(String filename) throws IOException {
		Table<String> table = loadCsv(filename);
//		Properties result = adaptToProperties(table);
		Map<String,String> result = adaptToProperties(table);
		return result;
	}
	
//	Properties adaptToProperties(Table<String> table) {
	Map<String,String> adaptToProperties(Table<String> table) {
//		Properties result = new Properties();
		Map<String,String> result = new LinkedHashMap<>();
//		for (String col : table.getColumnNames()) {
		for (String col : table.columnNames) {
//			System.out.println(col + ": " + 	// debug
//					table.getColumn(col));		// debug
			// Zimpl column indices start from 1
			int colIdx = table.header.get(col) + 1;
			// FIXME: allNumeral detection should be configurable.
			// TODO: could this be saved to avoid recomputation?
			boolean allNumeral = 
					Resolver.areAllNumeral(table.getColumn(col));
			
			// \w: word char: [a-zA-Z_0-9]
			// \p{IsAlphabetic}: an alphabetic char (Unicode)
//			Pattern pattern = Pattern.compile("(\\w+)\\[(.*?)\\]");  // \w+[\w+]
//			Matcher matcher = pattern.matcher(col);
//			boolean isIndexed = matcher.matches();
			// TODO: should an existing index be trimmed? should colname
			// be reported without index?
//			System.out.println("isIndexed: " + isIndexed); // debug
			
			String colDesc = Resolver
					.encode(table.source, colIdx, allNumeral);
			
//			result.setProperty(col, colDesc);
			result.put(col, colDesc);
		}
		return result;
	}
	
	Table<String> loadCsv(String filename) throws IOException {
		CsvParser parser = CsvParser.fromFilename(filename);
		ParseTree tree = parser.csvFile();
		CsvListener lstr = new CsvListener(filename);
		ParseTreeWalker.DEFAULT.walk(lstr, tree);
		return lstr.getResult();
	}
	
}
